package maps

import (
	"fmt"
	"math"
	"os"
	"os/exec"
)

func PrintMaps(state string) {
	fmt.Println(state)
}

func PrintMatrix(matrix [][]rune) {
	clrscr()
	fmt.Println("=====MAPS=====")
	for p := 0; p < len(matrix); p++ {
		fmt.Print("  ")
		for q := 0; q < len(matrix); q++ {
			fmt.Print(string(matrix[p][q]))
		}
		fmt.Println()
	}
	fmt.Println("==============")
}

func clrscr() {
	c := exec.Command("clear")
	c.Stdout = os.Stdout
	c.Run()
}

func MapsToMatrix(state string) [][]rune {
	array := mapsToArray(state)
	edgeLength := getEdgeLength(*array)
	matrix := makeEmptyMatrix(edgeLength)
	matrix = arrayToMatrix(*array, matrix)
	return matrix
}

func GetGoalPosition(matrix [][]rune) []int {
	for p := 0; p < len(matrix); p++ {
		for q := 0; q < len(matrix); q++ {
			if matrix[p][q] == 71 {
				return []int{p, q}
			}
		}
	}
	return []int{}
}

func mapsToArray(state string) *[]rune {
	array := make([]rune, 0)
	for _, v := range state {
		switch v {
		case 35:
			// Do Nothing
		case 10:
			// Do Nothing
		default:
			array = append(array, v)
		}
	}
	return &array
}

func getEdgeLength(array []rune) int {
	return int(math.Sqrt(float64(len(array))))
}

func makeEmptyMatrix(edgeLength int) [][]rune {
	matrix := make([][]rune, edgeLength)
	for i := range matrix {
		matrix[i] = make([]rune, edgeLength)
	}
	return matrix
}

func arrayToMatrix(array []rune, matrix [][]rune) [][]rune {
	j := 0
	for p := 0; p < getEdgeLength(array); p++ {
		for q := 0; q < getEdgeLength(array); q++ {
			matrix[p][q] = array[j]
			j++
		}
	}
	return matrix
}

package main

import (
	"fmt"

	"gitlab.com/farhanfarcun/boardgame-ai/agent"
	"gitlab.com/farhanfarcun/boardgame-ai/maps"
	"gitlab.com/farhanfarcun/boardgame-ai/move"
)

var initialState = `########################
#xxx&xxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxG#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#xxxxxxxxxxxxxxxxxxxxxx#
#x@xxxxxxxxxxxxxxxxxxxx#
########################`

func main() {
	matrix := maps.MapsToMatrix(initialState)
	maps.PrintMatrix(matrix)
	var input int
	var computerSkill int
	fmt.Println("Press Anything Except 99 To Play")
	fmt.Print(">")
	fmt.Scanf("%d", &input)
	fmt.Println("Choose Computer :")
	fmt.Println("1. Random")
	fmt.Println("2. Greedy")
	fmt.Print(">")
	fmt.Scanf("%d", &computerSkill)
	for {
		if input != 99 {
			var input1 string
			fmt.Println("WASD Rules | ctrl + c to exit")
			fmt.Print(">")
			fmt.Scanf("%s", &input1)

			switch input1 {
			case "w":
				move.MoveUp(&matrix, move.GetPositionPlayer(matrix))
			case "a":
				move.MoveLeft(&matrix, move.GetPositionPlayer(matrix))
			case "s":
				move.MoveDown(&matrix, move.GetPositionPlayer(matrix))
			case "d":
				move.MoveRigth(&matrix, move.GetPositionPlayer(matrix))
			}
			switch computerSkill {
			case 1:
				agent.RandomMove(&matrix)
			case 2:
				agent.GreedyMove(&matrix)
			default:
				agent.GreedyMove(&matrix)
			}
			maps.PrintMatrix(matrix)

		} else {
			break
		}
	}
}

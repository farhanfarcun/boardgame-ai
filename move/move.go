package move

import (
	"fmt"
	"os"
)

func GetPositionPlayer(matrix [][]rune) []int {
	for p := 0; p < len(matrix); p++ {
		for q := 0; q < len(matrix); q++ {
			if matrix[p][q] == 64 {
				return []int{p, q}
			}
		}
	}
	return []int{}
}

func GetPositionAgent(matrix [][]rune) []int {
	for p := 0; p < len(matrix); p++ {
		for q := 0; q < len(matrix); q++ {
			if matrix[p][q] == 38 {
				return []int{p, q}
			}
		}
	}
	return []int{}
}

func MoveUp(matrix *[][]rune, position []int) {
	flagGoal := false
	playerNextPos := []int{position[0] - 1, position[1]}
	if checkNextStep(position, 0, len(*matrix)) {
		if checkGoal(playerNextPos, *matrix) {
			flagGoal = true
		}
		*matrix = switchTiles(position, playerNextPos, *matrix)
		if flagGoal {
			fmt.Println("Game Over")
			os.Exit(0)
		}
	}
}

func MoveDown(matrix *[][]rune, position []int) {
	flagGoal := false
	playerNextPos := []int{position[0] + 1, position[1]}
	if checkNextStep(position, 1, len(*matrix)) {
		if checkGoal(playerNextPos, *matrix) {
			flagGoal = true
		}
		*matrix = switchTiles(position, playerNextPos, *matrix)
		if flagGoal {
			fmt.Println("Game Over")
			os.Exit(0)
		}
	}
}

func MoveLeft(matrix *[][]rune, position []int) {
	flagGoal := false
	playerNextPos := []int{position[0], position[1] - 1}
	if checkNextStep(position, 2, len(*matrix)) {
		if checkGoal(playerNextPos, *matrix) {
			flagGoal = true
		}
		*matrix = switchTiles(position, playerNextPos, *matrix)
		if flagGoal {
			fmt.Println("Game Over")
			os.Exit(0)
		}
	}
}

func MoveRigth(matrix *[][]rune, position []int) {
	flagGoal := false
	playerNextPos := []int{position[0], position[1] + 1}
	if checkNextStep(position, 3, len(*matrix)) {
		if checkGoal(playerNextPos, *matrix) {
			flagGoal = true
		}
		*matrix = switchTiles(position, playerNextPos, *matrix)
		if flagGoal {
			fmt.Println("Game Over")
			os.Exit(0)
		}
	}
}

func checkGoal(position []int, matrix [][]rune) bool {
	if matrix[position[0]][position[1]] == 71 {
		return true
	}
	return false
}

func switchTiles(current []int, will []int, matrix [][]rune) [][]rune {
	tempCurrent := matrix[current[0]][current[1]]
	tempWill := matrix[will[0]][will[1]]

	if tempWill == 71 {
		tempWill = 125
	}

	matrix[will[0]][will[1]] = tempCurrent
	matrix[current[0]][current[1]] = tempWill
	return matrix
}

func checkNextStep(playerPos []int, move int, size int) bool {
	conditionUp := playerPos[0] - 1    // 0
	conditionDown := playerPos[0] + 1  // 1
	conditionLeft := playerPos[1] - 1  // 2
	conditionRight := playerPos[1] + 1 // 3
	switch move {
	case 0:
		if conditionUp < 0 {
			return false
		}

	case 1:
		if conditionDown >= size {
			return false
		}
	case 2:
		if conditionLeft < 0 {
			return false
		}
	case 3:
		if conditionRight >= size {
			return false
		}
	default:
		return true
	}
	return true
}

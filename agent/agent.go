package agent

import (
	"math"
	"math/rand"
	"sort"

	"gitlab.com/farhanfarcun/boardgame-ai/maps"

	"gitlab.com/farhanfarcun/boardgame-ai/move"
)

func RandomMove(matrix *[][]rune) {
	randomNumber := rand.Intn(100)
	randomNumber %= 4
	switch randomNumber {
	case 0:
		move.MoveUp(matrix, move.GetPositionAgent(*matrix))
	case 1:
		move.MoveLeft(matrix, move.GetPositionAgent(*matrix))
	case 2:
		move.MoveDown(matrix, move.GetPositionAgent(*matrix))
	case 3:
		move.MoveRigth(matrix, move.GetPositionAgent(*matrix))
	}
}

func GreedyMove(matrix *[][]rune) {
	var arrayValue []int
	var value1, value2, value3, value4 int
	for i := 0; i < 4; i++ {
		switch i {
		case 0:
			agentPosition := move.GetPositionAgent(*matrix)
			agentPosition[0] -= 1
			value1 = heuristicManhattan(matrix, agentPosition)
			arrayValue = append(arrayValue, value1)
		case 1:
			agentPosition := move.GetPositionAgent(*matrix)
			agentPosition[1] -= 1
			value2 = heuristicManhattan(matrix, agentPosition)
			arrayValue = append(arrayValue, value2)
		case 2:
			agentPosition := move.GetPositionAgent(*matrix)
			agentPosition[0] += 1
			value3 = heuristicManhattan(matrix, agentPosition)
			arrayValue = append(arrayValue, value3)
		case 3:
			agentPosition := move.GetPositionAgent(*matrix)
			agentPosition[1] += 1
			value4 = heuristicManhattan(matrix, agentPosition)
			arrayValue = append(arrayValue, value4)
		}
	}
	sort.Ints(arrayValue)

	max := arrayValue[0]

	agentPosition := move.GetPositionAgent(*matrix)
	switch max {
	case value1:
		move.MoveUp(matrix, agentPosition)
	case value2:
		move.MoveLeft(matrix, agentPosition)
	case value3:
		move.MoveDown(matrix, agentPosition)
	case value4:
		move.MoveRigth(matrix, agentPosition)
	}
}

func heuristicManhattan(matrix *[][]rune, agentPosition []int) int {
	goalPosition := maps.GetGoalPosition(*matrix)
	heuristicNumber := math.Abs(float64(agentPosition[0])-float64(goalPosition[0])) + math.Abs(float64(agentPosition[1])-float64(goalPosition[1]))
	return int(heuristicNumber)
}
